# README
Prueba para desarrollador Laravel.

## Instrucciones de instalación
- Ejecutar los comandos `composer install` y `npm install` para instalar las dependencias necesarias.
- Crear el archivo `.env` con base en `.env.example`.
- Agregar la conexión a la base de datos en el archivo `.env`.
- Ejecutar el comando `php artisan key:generate` para generar la clave de la aplicación.
- Ejecutar el comando `php artisan storage:link` para agregar el acceso al almacenamiento.
- Ejecutar el comando `php artisan migrate --seed` para ejecutar las migraciones y sembrar los datos de prueba.
- Ejecutar el comando `php artisan serve` para ejecutar el backend del proyecto.
- En otra terminar, ejecutar el comando `npm run dev` para ejecutar el frontend del proyecto.
- Por defecto el proyecto es ejecutado en `http://localhost:8000/`.

## Tecnologías utilizadas
Este proyecto se construyó con las siguientes tecnologías:

- [Laravel 10](https://laravel.com)
- [Alpine JS](https://alpinejs.dev)

## Desarrollado por
Daniel Beltrán | Desarrollador Full-Stack