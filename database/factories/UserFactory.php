<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id'        => null,
            'role_id'        => rand(1, 2),
            'name'           => fake()->firstName(),
            'lastname'       => fake()->lastName(),
            'identification' => fake()->unique()->numerify('##########'),
            'address'        => fake()->streetAddress(),
            'phone_number'   => fake()->phoneNumber(),
            'country'        => fake()->country(),
            'city'           => fake()->city(),
            'email'          => fake()->unique()->safeEmail(),
            'password'       => 'password',
            'remember_token' => Str::random(10)
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}