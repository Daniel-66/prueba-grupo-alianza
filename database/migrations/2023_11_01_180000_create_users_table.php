<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->comment('ID del jefe')->nullable()->references('id')->on('users');
            $table->foreignId('role_id')->comment('ID del rol')->references('id')->on('roles');
            $table->string('name', 150)->comment('Nombre');
            $table->string('lastname', 150)->comment('Apellido');
            $table->string('identification', 15)->unique()->comment('Número de identificación');
            $table->string('address', 100)->nullable()->comment('Dirección');
            $table->string('phone_number', 100)->nullable()->comment('Número de teléfono');
            $table->string('country', 100)->nullable()->comment('País');
            $table->string('city', 100)->nullable()->comment('Ciudad');
            $table->string('email', 100)->unique()->comment('Correo electrónico');
            $table->string('password', 100)->comment('Contraseña');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};