<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name' => 'Jefe'],
            ['name' => 'Colaborador']
        ];

        foreach ($data as $item) {
            Role::factory()->create($item);
        }
    }
}