<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name' => 'Presidente'],
            ['name' => 'Director creativo'],
            ['name' => 'Community manager'],
            ['name' => 'Diseñador senior'],
            ['name' => 'Diseñador junior'],
            ['name' => 'Diseñador gráfico'],
            ['name' => 'Realizador visual']
        ];

        foreach ($data as $item) {
            Position::factory()->create($item);
        }
    }
}