<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'role_id'        => 1,
                'name'           => 'Daniel',
                'lastname'       => 'Beltrán',
                'identification' => '1051832785',
                'address'        => 'Calle 1 #1-1',
                'phone_number'   => '3183554144',
                'country'        => 'Colombia',
                'city'           => 'Bogotá',
                'email'          => 'daniel@example.com'
            ],
        ];

        foreach ($data as $item) {
            User::factory()->create($item);
        }

        User::factory(5)->create(['user_id' => 1]);
    }
}