<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'role_id',
        'name',
        'lastname',
        'identification',
        'address',
        'phone_number',
        'country',
        'city',
        'email',
        'password'
    ];

    protected $appends = [
        'fullname'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'remember_token'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed'
    ];

    /**
     * Obtener el valor del atributo fullname.
     *
     * @return string
     * @author Daniel Beltrán
     */
    public function getFullnameAttribute(): string {
        return $this->attributes['name'] . ' ' . $this->attributes['lastname'];
    }

    /**
     * Establecer el valor del atributo name.
     *
     * @param string $value Valor del atributo.
     * @return void
     * @author Daniel Beltrán
     */
    public function setNameAttribute(string $value): void {
        $this->attributes['name'] = mb_convert_case($value, MB_CASE_TITLE, 'utf-8');
    }

    /**
     * Establecer el valor del atributo lastname.
     *
     * @param string $value Valor del atributo.
     * @return void
     * @author Daniel Beltrán
     */
    public function setLastnameAttribute(string $value): void {
        $this->attributes['lastname'] = mb_convert_case($value, MB_CASE_TITLE, 'utf-8');
    }

    /**
     * Establecer el valor del atributo password.
     *
     * @param string $value Valor del atributo.
     * @return void
     * @author Daniel Beltrán
     */
    public function setPasswordAttribute(string $value): void {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Relación de muchos a uno con users.
     *
     * @return BelongsTo
     * @author Daniel Beltrán
     */
    public function boss(): BelongsTo {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Relación de muchos a uno con roles.
     *
     * @return BelongsTo
     * @author Daniel Beltrán
     */
    public function role(): BelongsTo {
        return $this->belongsTo(Role::class);
    }

    /**
     * Relación de muchos a muchos con positions.
     *
     * @return BelongsToMany
     * @author Daniel Beltrán
     */
    public function positions(): BelongsToMany {
        return $this->belongsToMany(Position::class)->withTimestamps();
    }

    /**
     * Relación de uno a muchos con users.
     *
     * @return HasMany
     * @author Daniel Beltrán
     */
    public function collaborators(): HasMany {
        return $this->hasMany(User::class, 'user_id');
    }
}