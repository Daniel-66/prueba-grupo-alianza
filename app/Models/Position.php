<?php

namespace App\Models;

use App\Http\Bases\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Position extends BaseModel
{
    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        'slug',
        'created_at',
        'updated_at'
    ];

    /**
     * Establecer el valor del atributo name.
     *
     * @param string $value Valor del atributo.
     * @return void
     * @author Daniel Beltrán
     */
    public function setNameAttribute(string $value): void {
        $this->attributes['name'] = ucfirst($value);
        $this->setSlugAttribute($value);
    }

    /**
     * Relación de muchos a muchos con users.
     *
     * @return BelongsToMany
     * @author Daniel Beltrán
     */
    public function users(): BelongsToMany {
        return $this->belongsToMany(User::class);
    }
}