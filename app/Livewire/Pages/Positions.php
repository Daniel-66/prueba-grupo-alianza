<?php

namespace App\Livewire\Pages;

use Livewire\Component;

class Positions extends Component
{
    public function render()
    {
        return view('livewire.pages.positions');
    }
}
