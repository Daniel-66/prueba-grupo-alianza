<?php

namespace App\Http\Requests;

use Illuminate\Support\Str;
use Illuminate\Foundation\Http\FormRequest;

class CreatePositionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Preparar datos para la validación.
     *
     * @return void
     * @author Daniel Beltrán
     */
    public function prepareForValidation(): void {
        $this->merge(['slug' => Str::slug($this->name)]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'slug' => 'required|string|max:80|unique:positions,slug,' . $this->id,
            'name' => 'required|string|max:80'
        ];
    }
}