<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Preparar datos para la validación.
     *
     * @return void
     * @author Daniel Beltrán
     */
    public function prepareForValidation(): void {
        if (gettype($this->positions) === 'string') {
            $this->merge(['positions' => json_decode($this->positions)]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'user_id'        => 'nullable|integer',
            'role_id'        => 'required|integer',
            'name'           => 'required|string|max:150',
            'lastname'       => 'required|string|max:150',
            'identification' => 'required|string|max:15|unique:users,identification,' . $this->id,
            'address'        => 'nullable|string|max:100',
            'phone_number'   => 'nullable|string|max:100',
            'country'        => 'nullable|string|max:100',
            'city'           => 'nullable|string|max:100',
            'email'          => 'required|email|max:100|unique:users,email,' . $this->id,
            'password'       => 'required|string|max:100',
            'positions'      => 'required|array|min:1',
            'positions.*'    => 'required|integer'
        ];

        if (isset($this->id) && !is_null($this->id)) {
            $rules['password'] = 'nullable|string|max:100';
        }

        return $rules;
    }
}