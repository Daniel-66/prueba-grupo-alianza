<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Support\Collection;

class RoleController extends Controller
{
    /**
     * Obtener roles.
     *
     * @return Collection
     * @author Daniel Beltrán
     */
    public static function findAll(): Collection {
        return Role::get();
    }
}