<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Requests\CreatePositionRequest;
use Illuminate\Pagination\LengthAwarePaginator;

class PositionController extends Controller
{
    /**
     * Obtener cargos.
     *
     * @param Request $request Filtros.
     * @return Collection|LengthAwarePaginator
     * @author Daniel Beltrán
     */
    public static function findAll(Request $request = new Request()): Collection | LengthAwarePaginator {
        $data = Position::orderBy('created_at', 'desc');

        if ($request->filled('results')) {
            $data = $data->paginate($request->results);
        } else {
            $data = $data->get();
        }

        return $data;
    }

    /**
     * Cargar página de cargos.
     *
     * @param Request $request Filtros.
     * @return View
     * @author Daniel Beltrán
     */
    public function index(Request $request): View {
        if (!$request->filled('results')) {
            $request->merge(['results' => 10]);
        }

        return view('livewire.pages.positions.index')->with([
            'positions' => $this->findAll($request)
        ]);
    }

    /**
     * Guardar cargo.
     *
     * @param CreatePositionRequest $request Contenido del formulario.
     * @return View
     * @author Daniel Beltrán
     */
    public function store(CreatePositionRequest $request): View {
        Position::create($request->all());

        return $this->index($request)->with([
            'message' => 'Cargo creado correctamente'
        ]);
    }

    /**
     * Actualizar cargo.
     *
     * @param int $id ID del cargo.
     * @param CreatePositionRequest $request Contenido del formulario.
     * @return View
     * @author Daniel Beltrán
     */
    public function update(int $id, CreatePositionRequest $request): View {
        Position::find($id)->update($request->all());

        return $this->index($request)->with([
            'message' => 'Cargo actualizado correctamente'
        ]);
    }

    /**
     * Eliminar cargo.
     *
     * @param int $id ID del cargo.
     * @return View
     * @author Daniel Beltrán
     */
    public function destroy(int $id): View {
        Position::find($id)->delete();

        return $this->index(new Request())->with([
            'message' => 'Cargo eliminado correctamente'
        ]);
    }
}