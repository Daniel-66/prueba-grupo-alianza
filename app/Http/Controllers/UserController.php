<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CreateUserRequest;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class UserController extends Controller
{
    /**
     * Obtener empleados.
     *
     * @param Request $request Filtros.
     * @return Collection|LengthAwarePaginator
     * @author Daniel Beltrán
     */
    public static function findAll(Request $request = new Request()): Collection | LengthAwarePaginator {
        $data = User::with([
            'boss:id,name,lastname',
            'role:id,name',
            'positions:id,name'
        ])
        ->when($request->filled('role_id'), function ($query) use ($request) {
            $query->where('role_id', $request->role_id);
        })
        ->orderBy('created_at', 'desc');

        if ($request->filled('results')) {
            $data = $data->paginate($request->results);
        } else {
            $data = $data->get();
        }

        return $data;
    }

    /**
     * Obtener empleado.
     *
     * @param int $id ID del empleado.
     * @return User|null
     * @author Daniel Beltrán
     */
    public static function findOne(int $id): User | null {
        return User::find($id);
    }

    /**
     * Cargar página de empleados.
     *
     * @param Request $request Filtros.
     * @return View
     * @author Daniel Beltrán
     */
    public function index(Request $request): View {
        if (!$request->filled('results')) {
            $request->merge(['results' => 10]);
        }

        return view('livewire.pages.employees.index')->with([
            'employees' => $this->findAll($request),
            'users'     => $this->findAll(new Request(['role_id' => 1])),
            'roles'     => RoleController::findAll(),
            'positions' => PositionController::findAll()
        ]);
    }

    /**
     * Guardar empleado.
     *
     * @param CreateUserRequest $request Contenido del formulario.
     * @return View
     * @author Daniel Beltrán
     */
    public function store(CreateUserRequest $request): View {
        try {
            DB::beginTransaction();
            $this->checkRole($request);
            $created = User::create($request->all());
            $created->positions()->sync($request->positions);
            DB::commit();

            $data = ['message' => 'Empleado creado correctamente'];
        } catch (Exception $error) {
            DB::rollback();
            $data = ['error_message' => 'Ocurrió un error al crear'];
        }

        return $this->index(new Request())->with($data);
    }

    /**
     * Actualizar empleado.
     *
     * @param int $id ID del empleado.
     * @param CreateUserRequest $request Contenido del formulario.
     * @return View
     * @author Daniel Beltrán
     */
    public function update(int $id, CreateUserRequest $request): View {
        try {
            DB::beginTransaction();
            $this->checkRole($request);
            $user = $this->findOne($id);
            $user->update($request->all());
            $user->positions()->sync($request->positions);
            DB::commit();

            $data = ['message' => 'Empleado actualizado correctamente'];
        } catch (Exception $error) {
            DB::rollback();
            $data = ['error_message' => 'Ocurrió un error al actualizar'];
        }

        return $this->index(new Request())->with($data);
    }

    /**
     * Eliminar empleado.
     *
     * @param int $id ID del empleado.
     * @return View
     * @author Daniel Beltrán
     */
    public function destroy(int $id): View {
        $user = $this->findOne($id);

        if (count($user->collaborators) == 0) {
            $user->positions()->detach();
            $user->delete();

            $data = ['message' => 'Empleado eliminado correctamente'];
        } else {
            $data = ['error_message' => 'No se puede eliminar este empleado, debido a que es jefe de otros empleados'];
        }

        return $this->index(new Request())->with($data);
    }

    /**
     * Validar el rol según los cargos que tenga.
     *
     * Verifica si alguno de los cargos asignados es el de presidente, en caso afirmativo, se asigna el rol de jefe.
     *
     * @param Request &$request Contenido del formulario.
     * @return void
     * @author Daniel Beltrán
     */
    private function checkRole(Request &$request): void {
        if (in_array(1, $request->positions)) {
            $request->merge(['role_id' => 1]);
        }
    }
}