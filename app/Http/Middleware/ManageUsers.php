<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ManageUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Solo los usuarios con rol de jefe pueden continuar
        if (Auth::user()->role_id == 1) {
            return $next($request);
        }

        return new JsonResponse([
            'message' => 'No tiene permiso para ejecutar esta acción'
        ], 403);
    }
}