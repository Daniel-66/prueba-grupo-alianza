<?php

namespace App\Http\Bases;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BaseModel extends Model
{
    use HasFactory;

    /**
     * Establecer el valor del atributo slug.
     *
     * @param string $value Valor del atributo.
     * @return void
     * @author Daniel Beltrán
     */
    public function setSlugAttribute(string $value): void {
        $this->attributes['slug'] = Str::slug($value);
    }
}