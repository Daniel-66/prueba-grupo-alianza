<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden">
                <div class="p-6 text-gray-900 text-center mt-8">
                    <h1 class="text-3xl">¡Bienvenid@ <br> {{ Auth::user()->fullname }}!</h1>

                    <p class="mt-20 mb-16">Añade los datos personales de tus empleados y después agrega su cargo en tu empresa</p>

                    <div>
                        <a class="text-xl text-blue-600 hover:bg-gray-400 hover:text-white p-3 rounded-full" href="javascript:void(0)">
                            <i class="fa-solid fa-user-plus"></i>
                        </a>

                        <span class="block mt-3">Empieza aquí</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="home-image"></div>
</x-app-layout>