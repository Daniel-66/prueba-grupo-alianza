@if (isset($message))
    <div class="py-4 px-3 bg-green-300 text-green-800 font-medium rounded mb-4">
        {{ $message }}
    </div>
@elseif ($errors->any())
    <div class="py-4 px-3 bg-red-300 text-red-800 font-medium rounded mb-4">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
