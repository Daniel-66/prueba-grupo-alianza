<div id="side-menu" class="bg-blue-600 text-white w-52 h-screen absolute h-screen">
    <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 64 64"
        aria-labelledby="title"
        aria-describedby="desc"
        role="img"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="30px"
        class="block m-auto mt-24 mb-16"
    >
        <path data-name="layer2"
        fill="#ffffff" d="M0 0h16v16H0zm24 0h16v16H24zm24 0h16v16H48zM0 48h16v16H0z"></path>
        <path data-name="layer1" fill="#ffffff" d="M24 48h16v16H24zm24 0h16v16H48z"></path>
        <path data-name="layer2" fill="#ffffff" d="M0 23.988h16v16H0z"></path>
        <path data-name="layer1" fill="#ffffff" d="M24 23.988h16v16H24zm24 0h16v16H48z"></path>
    </svg>

    <a
        href="{{ route('home') }}"
        class="font-semibold pl-8 py-1.5 text-lg block {{ Route::currentRouteName() == 'home' ? 'bg-white-rgba' : '' }}"
    >
        Home
    </a>

    <div x-data="{ open: false }">
        <a
            href="javascript:void(0)"
            class="font-semibold pl-8 py-1.5 text-lg block mt-5 {{ Route::currentRouteName() != 'home' ? 'bg-white-rgba' : '' }} flex items-center"
            @click="open = !open"
        >
            Listas
            <template x-if="!open">
                <i class="fa fa-chevron-up absolute right-4 text-sm"></i>
            </template>

            <template x-if="open">
                <i class="fa fa-chevron-down absolute right-4 text-sm"></i>
            </template>
        </a>

        <ul class="ml-9 mt-2 transition-all" x-show="open">
            <li class="hover:bg-gradient-to-r from-blue-700 to-emerald-800 py-1 pl-2 transition-all {{ Route::currentRouteName() == 'positions' ? 'bg-gradient-to-r from-blue-700 to-emerald-800' : '' }}">
                <a href="{{ route('positions') }}" class="w-full block">Cargos</a>
            </li>

            <li class="hover:bg-gradient-to-r from-blue-700 to-emerald-800 py-1 pl-2 transition-all {{ Route::currentRouteName() == 'employees' ? 'bg-gradient-to-r from-blue-700 to-emerald-800' : '' }}">
                <a href="{{ route('employees') }}" class="w-full block">Empleados</a>
            </li>
        </ul>
    </div>
</div>