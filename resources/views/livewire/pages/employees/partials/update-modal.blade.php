<div x-data="updateComponent()">
    <x-modal name="update-modal">
        <x-slot name="slot">
            <div class="relative rounded-lg shadow">
                <!-- Header -->
                <div class="flex items-start justify-between p-4 border-b rounded-t bg-blue-600 shadow-md">
                    <h3 class="text-lg text-white">Editar Empleado</h3>

                    <button
                        type="button"
                        class="text-gray-400 hover:bg-gray-300 hover:text-white hover:border-gray-300 rounded-full text-sm w-8 h-8 ml-auto inline-flex justify-center items-center border border-3 border-gray-400 transition-all"
                        @click="$dispatch('close')"
                    >
                        <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                        </svg>
                        <span class="sr-only">Cerrar</span>
                    </button>
                </div>

                <!-- Body -->
                <div class="p-6 space-y-6">
                    <form>
                        <input type="hidden" id="update_id" name="id" :value="employee?.id">

                        <div class="grid grid-cols-2">
                            <div class="px-6">
                                <label for="update_name" class="block mb-2 text-sm font-medium text-gray-900">Nombres</label>

                                <input
                                    type="text"
                                    id="update_name"
                                    name="name"
                                    :value="employee?.name"
                                    class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                    required
                                    autocomplete="off"
                                    placeholder="Escribe el nombre de tu empleado"
                                >
                            </div>

                            <div class="px-6">
                                <label for="update_lastname" class="block mb-2 text-sm font-medium text-gray-900">Apellidos</label>

                                <input
                                    type="text"
                                    id="update_lastname"
                                    name="lastname"
                                    :value="employee?.lastname"
                                    class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                    required
                                    autocomplete="off"
                                    placeholder="Escribe los apellidos de tu empleado"
                                >
                            </div>
                        </div>

                        <div class="grid grid-cols-2 mt-4">
                            <div class="px-6">
                                <label for="update_identification" class="block mb-2 text-sm font-medium text-gray-900">Identificación</label>

                                <input
                                    type="text"
                                    id="update_identification"
                                    name="identification"
                                    :value="employee?.identification"
                                    class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                    required
                                    autocomplete="off"
                                    placeholder="Escribe un número de identificación"
                                >
                            </div>

                            <div class="px-6">
                                <label for="update_phone_number" class="block mb-2 text-sm font-medium text-gray-900">Teléfono</label>

                                <input
                                    type="text"
                                    id="update_phone_number"
                                    name="phone_number"
                                    :value="employee?.phone_number"
                                    class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                    autocomplete="off"
                                    placeholder="Escribe un número de teléfono"
                                >
                            </div>
                        </div>

                        <div class="grid grid-cols-2 mt-4">
                            <div class="px-6">
                                <label for="update_country" class="block mb-2 text-sm font-medium text-gray-900">País</label>

                                <select
                                    id="update_country"
                                    name="country"
                                    :value="employee?.country"
                                    class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                    @change="searchCities()"
                                    x-model="country_selected"
                                >
                                    <option value="{{ null }}" selected>Selecciona un país</option>

                                    <template x-for="item in countries">
                                        <option :value="item" x-text="item"></option>
                                    </template>
                                </select>
                            </div>

                            <div class="px-6">
                                <label for="update_city" class="block mb-2 text-sm font-medium text-gray-900">Ciudad</label>

                                <select
                                    id="update_city"
                                    name="city"
                                    class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                    @click="searchCities()"
                                >
                                    <option value="{{ null }}" selected>Selecciona una ciudad</option>

                                    <template x-for="item in cities_of_country">
                                        <option :value="item.name" :selected="item.name == employee?.city" x-text="item.name"></option>
                                    </template>
                                </select>
                            </div>
                        </div>

                        <div class="grid grid-cols-2 mt-4">
                            <div class="px-6">
                                <label for="update_address" class="block mb-2 text-sm font-medium text-gray-900">Dirección</label>

                                <input
                                    type="text"
                                    id="update_address"
                                    name="address"
                                    :value="employee?.address"
                                    class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                    autocomplete="off"
                                    placeholder="Escribe la dirección de tu empleado"
                                >
                            </div>

                            <div class="px-6">
                                <label for="update_email" class="block mb-2 text-sm font-medium text-gray-900">Correo electrónico</label>

                                <input
                                    type="email"
                                    id="update_email"
                                    name="email"
                                    :value="employee?.email"
                                    class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                    required
                                    autocomplete="off"
                                    placeholder="Escribe un correo electrónico"
                                >
                            </div>
                        </div>

                        <div class="grid grid-cols-2 mt-4">
                            <div class="px-6">
                                <label for="update_user_id" class="block mb-2 text-sm font-medium text-gray-900">Jefe</label>

                                <select
                                    id="update_user_id"
                                    name="user_id"
                                    :value="employee?.user_id"
                                    class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                >
                                    <option value="{{ null }}" selected>Selecciona un jefe</option>

                                    @foreach ($users as $item)
                                    <option value="{{ $item->id }}">{{ $item->fullname }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="px-6">
                                <label for="update_role_id" class="block mb-2 text-sm font-medium text-gray-900">Rol</label>

                                <select
                                    id="update_role_id"
                                    name="role_id"
                                    :value="employee?.role_id"
                                    class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                    required
                                >
                                    <option value="{{ null }}" selected>Selecciona un rol</option>

                                    @foreach ($roles as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="grid grid-cols-2 mt-4">
                            <div class="px-6">
                                <label for="update_positions" class="block mb-2 text-sm font-medium text-gray-900">Cargos</label>

                                <select
                                    multiple
                                    id="update_positions"
                                    name="positions"
                                    :value="employee?.positions"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                    required
                                >
                                    @foreach ($positions as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            {{-- <div class="px-6">
                                <label for="update_password" class="block mb-2 text-sm font-medium text-gray-900">Contraseña</label>

                                <input
                                    type="password"
                                    id="update_password"
                                    name="password"
                                    class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                    required
                                    autocomplete="off"
                                    placeholder="Escribe una contraseña"
                                >
                            </div> --}}
                        </div>

                        <div class="flex items-center justify-center pt-12 pb-3 space-x-2">
                            <button
                                type="button"
                                class="text-gray-600 bg-gray-300 font-bold focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-full border border-gray-200 text-sm px-5 py-2 hover:bg-blue-500 hover:text-white focus:z-10 mr-6"
                                @click="$dispatch('close')"
                            >
                                Cancelar
                            </button>

                            <button type="button" class="text-white bg-blue-700 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-bold rounded-full text-sm px-5 py-2 text-center" @click="update()">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </x-slot>
    </x-modal>
</div>

<script>
    function updateComponent() {
        return {
            update: function() {
                const id = this.employee?.id;

                let data = new FormData();
                data.append('id', document.getElementById('update_id').value);
                data.append('name', document.getElementById('update_name').value);
                data.append('lastname', document.getElementById('update_lastname').value);
                data.append('identification', document.getElementById('update_identification').value);
                data.append('phone_number', document.getElementById('update_phone_number').value);
                data.append('country', document.getElementById('update_country').value);
                data.append('city', document.getElementById('update_city').value);
                data.append('address', document.getElementById('update_address').value);
                data.append('email', document.getElementById('update_email').value);
                data.append('user_id', document.getElementById('update_user_id').value);
                data.append('role_id', document.getElementById('update_role_id').value);
                data.append('positions', this.getSelectedItems());
                data.append('_method', 'PUT');


                data.forEach((value, key) => {
                    console.log(key + ': ' + value)
                });

                const request = {
                    headers: {
                        'X-CSRF-TOKEN'   : '{{ csrf_token() }}',
                        'Accept-Encoding': 'gzip, deflate, br',
                        Accept           : 'application/json'
                    },
                    method: 'POST',
                    body  : data
                };

                fetch('/employees/' + id, request)
                .then(r => console.log(r))
                .catch(e => console.log(e))
                .finally(() => {
                    location.href = '/employees';
                });
            },
            getSelectedItems: function () {
                const select       = document.getElementsByName('positions')[0];
                const options      = select && select.options;
                let items_selected = [];

                for (let i = 0; i < options.length; i++) {
                    if (options[i].selected) {
                        items_selected.push(options[i].value || options[i].text);
                    }
                }

                return JSON.stringify(items_selected);
            }
        }
    }
</script>