<x-modal name="create-modal">
    <x-slot name="slot">
        <div class="relative rounded-lg shadow">
            <!-- Header -->
            <div class="flex items-start justify-between p-4 border-b rounded-t bg-gray-100 shadow-md">
                <h3 class="text-lg text-gray-500">Nuevo Empleado</h3>

                <button
                    type="button"
                    class="text-gray-400 hover:bg-gray-300 hover:text-white hover:border-gray-300 rounded-full text-sm w-8 h-8 ml-auto inline-flex justify-center items-center border border-3 border-gray-400 transition-all"
                    @click="$dispatch('close')"
                >
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                    </svg>
                    <span class="sr-only">Cerrar</span>
                </button>
            </div>

            <!-- Body -->
            <div class="p-6 space-y-6">
                <form action="{{ route('employees') }}" method="POST">
                    @csrf

                    <div class="grid grid-cols-2">
                        <div class="px-6">
                            <label for="name" class="block mb-2 text-sm font-medium text-gray-900">Nombres</label>

                            <input
                                type="text"
                                id="name"
                                name="name"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                required
                                autocomplete="off"
                                placeholder="Escribe el nombre de tu empleado"
                            >
                        </div>

                        <div class="px-6">
                            <label for="lastname" class="block mb-2 text-sm font-medium text-gray-900">Apellidos</label>

                            <input
                                type="text"
                                id="lastname"
                                name="lastname"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                required
                                autocomplete="off"
                                placeholder="Escribe los apellidos de tu empleado"
                            >
                        </div>
                    </div>

                    <div class="grid grid-cols-2 mt-4">
                        <div class="px-6">
                            <label for="identification" class="block mb-2 text-sm font-medium text-gray-900">Identificación</label>

                            <input
                                type="text"
                                id="identification"
                                name="identification"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                required
                                autocomplete="off"
                                placeholder="Escribe un número de identificación"
                            >
                        </div>

                        <div class="px-6">
                            <label for="phone_number" class="block mb-2 text-sm font-medium text-gray-900">Teléfono</label>

                            <input
                                type="text"
                                id="phone_number"
                                name="phone_number"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                autocomplete="off"
                                placeholder="Escribe un número de teléfono"
                            >
                        </div>
                    </div>

                    <div class="grid grid-cols-2 mt-4">
                        <div class="px-6">
                            <label for="country" class="block mb-2 text-sm font-medium text-gray-900">País</label>

                            <select
                                id="country"
                                name="country"
                                class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                @change="searchCities()"
                                x-model="country_selected"
                            >
                                <option value="{{ null }}" selected>Selecciona un país</option>

                                <template x-for="item in countries">
                                    <option :value="item" x-text="item"></option>
                                </template>
                            </select>
                        </div>

                        <div class="px-6">
                            <label for="city" class="block mb-2 text-sm font-medium text-gray-900">Ciudad</label>

                            <select
                                id="city"
                                name="city"
                                class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                            >
                                <option value="{{ null }}" selected>Selecciona una ciudad</option>

                                <template x-for="item in cities_of_country">
                                    <option :value="item.name" x-text="item.name"></option>
                                </template>
                            </select>
                        </div>
                    </div>

                    <div class="grid grid-cols-2 mt-4">
                        <div class="px-6">
                            <label for="address" class="block mb-2 text-sm font-medium text-gray-900">Dirección</label>

                            <input
                                type="text"
                                id="address"
                                name="address"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                autocomplete="off"
                                placeholder="Escribe la dirección de tu empleado"
                            >
                        </div>

                        <div class="px-6">
                            <label for="email" class="block mb-2 text-sm font-medium text-gray-900">Correo electrónico</label>

                            <input
                                type="email"
                                id="email"
                                name="email"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                required
                                autocomplete="off"
                                placeholder="Escribe un correo electrónico"
                            >
                        </div>
                    </div>

                    <div class="grid grid-cols-2 mt-4">
                        <div class="px-6">
                            <label for="user_id" class="block mb-2 text-sm font-medium text-gray-900">Jefe</label>

                            <select
                                id="user_id"
                                name="user_id"
                                class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                            >
                                <option value="{{ null }}" selected>Selecciona un jefe</option>

                                @foreach ($users as $item)
                                <option value="{{ $item->id }}">{{ $item->fullname }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="px-6">
                            <label for="role_id" class="block mb-2 text-sm font-medium text-gray-900">Rol</label>

                            <select
                                id="role_id"
                                name="role_id"
                                class="border border-gray-300 text-gray-900 text-xs rounded-full focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                required
                            >
                                <option value="{{ null }}" selected>Selecciona un rol</option>

                                @foreach ($roles as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="grid grid-cols-2 mt-4">
                        <div class="px-6">
                            <label for="positions" class="block mb-2 text-sm font-medium text-gray-900">Cargos</label>

                            <select
                                multiple
                                id="positions"
                                name="positions[]"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2"
                                required
                            >
                                @foreach ($positions as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="px-6">
                            <label for="password" class="block mb-2 text-sm font-medium text-gray-900">Contraseña</label>

                            <input
                                type="password"
                                id="password"
                                name="password"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                required
                                autocomplete="off"
                                placeholder="Escribe una contraseña"
                            >
                        </div>
                    </div>

                    <div class="flex items-center justify-center pt-12 pb-3 space-x-2">
                        <button
                            type="button"
                            class="text-gray-600 bg-gray-300 font-bold focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-full border border-gray-200 text-sm px-5 py-2 hover:bg-blue-500 hover:text-white focus:z-10 mr-6"
                            @click="$dispatch('close')"
                        >
                            Cancelar
                        </button>

                        <button type="submit" class="text-white bg-blue-700 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-bold rounded-full text-sm px-5 py-2 text-center">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </x-slot>
</x-modal>