<x-app-layout>
    <x-slot name="header">{{ __('Empleados') }}</x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div x-data="mainComponent()">
                <div class="p-6 text-gray-900">
                    <div class="mb-6">
                        <button type="button" class="text-blue-600 mx-3 py-1 px-3 font-bold hover:bg-gray-200 hover:rounded-full">
                            <i class="fa-solid fa-trash mr-1"></i> Borrar selección
                        </button>

                        <button type="button" class="text-blue-600 mx-3 py-1 px-3 font-bold hover:bg-gray-200 hover:rounded-full">
                            <i class="fa-solid fa-file-arrow-down mr-1"></i> Descargar datos
                        </button>

                        <div x-data class="inline">
                            <button
                                type="button"
                                @click="$dispatch('open-modal', 'create-modal')"
                                class="float-right text-blue-600 mx-3 py-1 px-3 font-bold rounded-full border-solid border-2 border-blue-500 hover:bg-gray-100"
                            >
                                <i class="fa-solid fa-briefcase mr-1"></i> Agregar
                            </button>
                        </div>
                    </div>

                    @include('layouts.alert-message')

                    <div class="relative w-full overflow-x-auto">
                        <table class="table-auto border-separate border-spacing-y-2 w-full" x-data="{ checked: false }">
                            <thead>
                                <tr>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 py-3 text-left rounded-s whitespace-nowrap">
                                        <label for="mark-all" class="ml-3 mr-2 text-sm">Todos</label>
                                        <input type="checkbox" id="mark-all" class="appearance-none checked:bg-blue-500" x-model="checked">
                                    </th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Nombre</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Identificación</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Dirección</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Teléfono</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">País</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Ciudad</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Cargo</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Rol</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left pl-3">Jefe</th>
                                    <th class="bg-gray-200 border border-1 border-gray-200 h-12 text-sm py-3 text-left px-3 rounded-e">Acciones</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($employees as $item)
                                <tr>
                                    <td class="text-center border border-e-0 border-gray-200 rounded-s py-2">
                                        <input type="checkbox" class="appearance-none checked:bg-blue-500" :checked="checked">
                                    </td>

                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1 whitespace-nowrap">{{ $item->fullname }}</td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1">{{ $item->identification }}</td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1">{{ $item->address }}</td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1">{{ $item->phone_number }}</td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1">{{ $item->country }}</td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1">{{ $item->city }}</td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1">
                                        @if (count($item->positions) > 0)
                                            {{ $item->positions[0]->name }}
                                        @endif
                                    </td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1">{{ $item->role->name }}</td>
                                    <td class="border border-x-0 border-gray-200 py-2 text-sm px-1 whitespace-nowrap">{{ $item->boss?->fullname }}</td>

                                    <td class="text-center border border-s-0 border-gray-200 rounded-e py-2 px-1 whitespace-nowrap">
                                        <button
                                            type="button"
                                            class="text-blue-600 hover:text-gray-400 mx-1.5 inline-block text-lg"
                                            @click="openModal({{ $item }}, 'update', $dispatch)"
                                        >
                                            <i class="fa-solid fa-pen"></i>
                                        </button>

                                        <button
                                            type="button"
                                            class="text-blue-600 hover:text-gray-400 mx-1.5 inline-block text-lg"
                                            @click="openModal({{ $item }}, 'delete', $dispatch)"
                                        >
                                            <i class="fa-solid fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="mt-4">
                        <div x-data="{ open: false }" class="absolute w-fit">
                            <button
                                type="button"
                                class="flex items-center px-5 py-2.5 text-sm"
                                @click="open = !open"
                            >
                                Mostrar de a

                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 ml-1 text-blue-600" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </button>

                            <ul
                                x-show="open"
                                @click.away="open = false"
                                class="bg-gray-100 w-40 text-center rounded-sm absolute start-full top-0 z-10"
                            >
                                <li class="w-full">
                                    <a
                                        class="w-full block p-2 hover:bg-blue-600 hover:text-white rounded-sm transition-all text-sm"
                                        href="{{ route('employees', ['results' => 10]) }}"
                                    >
                                        10 resultados
                                    </a>
                                </li>

                                <li class="w-full">
                                    <a
                                        class="w-full block p-2 hover:bg-blue-600 hover:text-white rounded-sm transition-all text-sm"
                                        href="{{ route('employees', ['results' => 30]) }}"
                                    >
                                        30 resultados
                                    </a>
                                </li>

                                <li class="w-full">
                                    <a
                                        class="w-full block p-2 hover:bg-blue-600 hover:text-white rounded-sm transition-all text-sm"
                                        href="{{ route('employees', ['results' => 50]) }}"
                                    >
                                        50 resultados
                                    </a>
                                </li>
                            </ul>
                        </div>

                        {{ $employees->links() }}
                    </div>
                </div>

                @include('livewire.pages.employees.partials.create-modal')
                @include('livewire.pages.employees.partials.update-modal')
                @include('livewire.pages.employees.partials.delete-modal')
            </div>
        </div>
    </div>
</x-app-layout>


<script>
    function mainComponent() {
        return {
            employee: null,
            countries: ['Colombia', 'Argentina', 'Brasil', 'Estados Unidos', 'España', 'Italia', 'Rusia', 'Sudafrica', 'Australia'],
            country_selected: null,
            cities: [
                { country: 'Colombia', name: 'Bogotá' },
                { country: 'Colombia', name: 'Cali' },
                { country: 'Colombia', name: 'Medellín' },
                { country: 'Colombia', name: 'Tunja' },
                { country: 'Colombia', name: 'Barranquilla' },
                { country: 'Argentina', name: 'Buenos Aires' },
                { country: 'Argentina', name: 'Rosario' },
                { country: 'Argentina', name: 'Córdoba' },
                { country: 'Brasil', name: 'Sao Pablo' },
                { country: 'Brasil', name: 'Bahía' },
                { country: 'Brasil', name: 'Curitiba' },
                { country: 'Estados Unidos', name: 'Chicago' },
                { country: 'Estados Unidos', name: 'New York' },
                { country: 'Estados Unidos', name: 'Los Angeles' },
                { country: 'Estados Unidos', name: 'Miami' },
                { country: 'España', name: 'Valencia' },
                { country: 'España', name: 'Madrid' },
                { country: 'España', name: 'Barcelona' },
                { country: 'Italia', name: 'Milán' },
                { country: 'Italia', name: 'Napoles' },
                { country: 'Italia', name: 'Venecia' },
                { country: 'Rusia', name: 'Moscú' },
                { country: 'Rusia', name: 'Novgorod' },
                { country: 'Sudafrica', name: 'Ciudad del cabo' },
                { country: 'Australia', name: 'Sidney' },
                { country: 'Australia', name: 'Perth' }
            ],
            cities_of_country: [],
            openModal: function (employee, modal_name, $dispatch) {
                this.employee = employee;

                if (modal_name === 'update') {
                    this.country_selected  = null;
                    this.cities_of_country = this.cities;

                    if (this.employee?.country) {
                        this.country_selected = this.employee.country;
                    }

                    if (this.employee?.positions.length > 0) {
                        let positions_selected = [];

                        this.employee.positions.forEach(p => {
                            positions_selected.push(p.id);
                        });

                        this.employee.positions = positions_selected;
                    }
                }

                modal_name += '-modal';
                $dispatch('open-modal', modal_name);
            },
            searchCities: function () {
                this.cities_of_country = this.cities.filter((c) => c.country == this.country_selected);
            }
        }
    }
</script>