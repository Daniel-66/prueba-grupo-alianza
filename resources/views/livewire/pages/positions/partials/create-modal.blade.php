<x-modal name="create-modal">
    <x-slot name="slot">
        <div class="relative rounded-lg shadow">
            <!-- Header -->
            <div class="flex items-start justify-between p-4 border-b rounded-t bg-gray-100 shadow-md">
                <h3 class="text-lg text-gray-500">Nuevo Cargo</h3>

                <button
                    type="button"
                    class="text-gray-400 hover:bg-gray-300 hover:text-white hover:border-gray-300 rounded-full text-sm w-8 h-8 ml-auto inline-flex justify-center items-center border border-3 border-gray-400 transition-all"
                    @click="$dispatch('close')"
                >
                    <svg class="w-3 h-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                    </svg>
                    <span class="sr-only">Cerrar</span>
                </button>
            </div>

            <!-- Body -->
            <div class="p-6 space-y-6">
                <form action="{{ route('positions') }}" method="POST">
                    @csrf

                    <div class="grid grid-cols-1">
                        <div class="px-6">
                            <label for="name" class="block mb-2 text-sm font-medium text-gray-900">Cargo</label>

                            <input
                                type="text"
                                id="name"
                                name="name"
                                class="block w-full p-2 text-gray-900 border border-gray-300 rounded-full sm:text-xs focus:ring-blue-500 focus:border-blue-500"
                                required
                                autocomplete="off"
                                placeholder="Escribe el nombre del cargo"
                            >
                        </div>
                    </div>

                    <div class="flex items-center justify-center pt-12 pb-3 space-x-2">
                        <button
                            type="button"
                            class="text-gray-600 bg-gray-300 font-bold focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-full border border-gray-200 text-sm px-5 py-2 hover:bg-blue-500 hover:text-white focus:z-10 mr-6"
                            @click="$dispatch('close')"
                        >
                            Cancelar
                        </button>

                        <button type="submit" class="text-white bg-blue-700 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-bold rounded-full text-sm px-5 py-2 text-center">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </x-slot>
</x-modal>