<div x-data="deleteComponent()">
    <x-modal name="delete-modal">
        <x-slot name="slot">
            <div class="relative rounded-lg shadow">
                <!-- Header -->
                <div class="flex items-center justify-center p-4 rounded-t">
                    <h3 class="text-center mt-20">
                        <i class="fa-solid fa-trash text-4xl text-blue-600"></i>
                        <div class="text-3xl mt-5">Borrar Cargo</div>
                    </h3>
                </div>

                <!-- Body -->
                <div class="p-6 space-y-6">
                    <div class="grid grid-cols-1 text-center">
                        <div class="px-6">
                            ¿Está seguro de borrar el cargo de <span x-text="position?.name"></span>?
                        </div>
                    </div>

                    <div class="flex items-center justify-center pt-12 pb-3 space-x-2">
                        <button
                            type="button"
                            class="text-white bg-blue-700 hover:bg-blue-500 focus:ring-4 focus:outline-none focus:ring-blue-300 font-bold rounded-full text-sm px-5 py-2"
                            @click="$dispatch('close')"
                        >
                            Cancelar
                        </button>

                        <button type="button" class="text-gray-600 bg-gray-300 font-bold focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-full border border-gray-200 text-sm px-5 py-2 hover:bg-blue-500 hover:text-white focus:z-10 mr-6 transition-all" @click="remove()">Aceptar</button>
                    </div>
                </div>
            </div>
        </x-slot>
    </x-modal>
</div>

<script>
    function deleteComponent() {
        return {
            remove: function() {
                const id = this.position?.id;

                let data = new FormData();
                data.append('_method', 'DELETE');

                const request = {
                    headers: {
                        'X-CSRF-TOKEN'   : '{{ csrf_token() }}',
                        'Accept-Encoding': 'gzip, deflate, br',
                        Accept           : 'application/json'
                    },
                    method: 'POST',
                    body  : data
                };

                fetch('/positions/' + id, request)
                .then(r => console.log(r))
                .catch(e => console.log(e))
                .finally(() => {
                    location.href = '/positions';
                });
            }
        }
    }
</script>