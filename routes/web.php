<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PositionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group([
    'middleware' => 'auth'
], function ($route) {
    $route->get('/home', function () {
        return view('dashboard');
    })->name('home');

    $route->get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    $route->patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    $route->delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    $route->get('roles', [RoleController::class, 'index'])->name('roles');
    $route->resource('positions', PositionController::class, ['except'=>'destroy'])->name('index', 'positions');
    $route->get('employees', [UserController::class, 'index'])->name('employees');
    $route->post('employees', [UserController::class, 'store'])->name('employees.store');

    $route->group([
        'middleware' => 'manage.users'
    ], function ($route) {
        $route->put('employees/{id}', [UserController::class, 'update'])->name('employees.update');
        $route->delete('employees/{id}', [UserController::class, 'destroy'])->name('employees.destroy');
    });
});

require __DIR__.'/auth.php';